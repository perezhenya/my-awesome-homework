
  /*

    Задание 1.

    Написать скрипт который будет будет переключать вкладки по нажатию
    на кнопки в хедере.

    Главное условие - изменять файл HTML нельзя.

    Алгоритм:
      1. Выбрать каждую кнопку в шапке
         + бонус выбрать одним селектором

      2. Повесить кнопку онклик
          button1.onclick = function(event) {

          }
          + бонус: один обработчик на все три кнопки

      3. Написать функцию которая выбирает соответствующую вкладку
         и добавляет к ней класс active

      4. Написать функцию hideAllTabs которая прячет все вкладки.
         Удаляя класс active со всех вкладок

    Методы для работы:

      getElementById
      querySelector
      classList
      classList.add
      forEach
      onclick

    
  */
var btnContainer = document.getElementById('buttonContainer');
var buttons = btnContainer.querySelectorAll('.showButton');
var tabContainer = document.getElementById('tabContainer');

buttons.forEach(button => {
   button.addEventListener('click', showTabs);
   button.addEventListener('click', hideAllTabs);
})

function showTabs(e) {
let buttonTarget = e.target.dataset.tab;
// console.log( buttonTarget )
let dataTab = tabContainer.querySelectorAll('.tab');
dataTab.forEach(tab => {
if(tab.dataset.tab === buttonTarget) {
   tab.classList.add('active');
   // console.log(tab.classList)
}
 
})
}

function hideAllTabs(e) {
let buttonTarget = e.target.dataset.tab;
// console.log( buttonTarget )
let dataTab = tabContainer.querySelectorAll('.tab');
dataTab.forEach(tab => {
if(tab.dataset.tab !== buttonTarget) {
   tab.classList.remove('active');
   // console.log(tab.classList)
}
})
}


 

