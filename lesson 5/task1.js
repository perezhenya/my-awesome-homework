/*

    Задание 1:

    Написать обьект Train у которого будут свойства:
    -имя,
    -скорость езды
    -количество пассажиров
    Методы:
    Ехать -> Поезд {name} везет { количество пассажиров} со скоростью {speed}
    Стоять -> Поезд {name} остановился. Скорость {speed}
    Подобрать пассажиров -> Увеличивает кол-во пассажиров на Х
*/
var Train = {};
    Train.name =  'intercity';
    Train.speed = 100;
    Train.passengers = 1000;
    Train.go = function() {
        console.log(this.name + ' везет ' + this.passengers +' пассажиров ' + ' со скоростью ' + this.speed + ' км/ч' );

    };
    Train.stop = function() {
        this.speed = 0;
        console.log(this.name  + ' остановился ' + '.' + ' скорость ' + this.speed );

    }
    Train.newPassengers = function(x) {
        this.passengers+= x;
        console.log(this.passengers)
    }
console.log(Train.go())
console.log(Train.stop())
console.log(Train.newPassengers(), 20);


